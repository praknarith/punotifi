//
//  HomeTableViewCell.swift
//  PuNotifi
//
//  Created by Narith on 1/22/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    

    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var frameView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
