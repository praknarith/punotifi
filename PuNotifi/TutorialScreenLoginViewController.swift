//
//  TutorialScreenLoginViewController.swift
//  PuNotifi
//
//  Created by Narith on 1/22/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import Firebase
import FBSDKCoreKit



class TutorialScreenLoginViewController: UIViewController {

    var ref: FIRDatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        ref = FIRDatabase.database().reference()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    @IBAction func facebookLoginButton(_ sender: UIButton) {
        loginButtonClicked()
    }
    
    @IBAction func skipButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "gotoHomeLogin", sender: nil)
    }
    
    func saveUserInfo(_ user: FIRUser, withUsername username: String) {
        //create change request
        let changeRequest = FIRAuth.auth()?.currentUser?.profileChangeRequest()
        changeRequest?.displayName = username
        
        self.ref.child("users").child(user.uid).setValue(["username": username])
    }
    
    @objc func loginButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logIn([.publicProfile, .email, .custom("user_photos")], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _,  _,let accessToken):
                print("Logged in!")
                let credential = FIRFacebookAuthProvider.credential(withAccessToken: accessToken.authenticationToken)
                FIRAuth.auth()?.signIn(with: credential) { (user, error) in
                    if let error = error {
                        print(error)
                        return
                    }
                let user = FIRAuth.auth()?.currentUser
                let name = user?.displayName
                let email = user?.email
                let photo = user?.photoURL?.absoluteString
                guard let uid = user?.uid else{
                    return
                }
                let ref = FIRDatabase.database().reference(fromURL: "https://punotifi.firebaseio.com/")
                let userReference = ref.child("users").child(uid)

                let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, photoURL"])
                graphRequest.start(completionHandler: { (connection, result, error) -> Void in
                    let values: Dictionary<String,Any> = [
                        "name": name!,
                        "email": email!,
                        "photo": photo!
                    ]
                userReference.updateChildValues(values) { (err, ref) in
                        if err != nil{
                            print(err!)
                        }
                    }
                })
    
            }
            
            }
                NotificationCenter.default.post(name: Notification.Name(rawValue: "finishLogin"), object: nil, userInfo: nil)
                self.performSegue(withIdentifier: "gotoHomeLogin", sender: nil)
            }
        }
    }
    
    fileprivate func registerFBUser(_ uid: String, value:[String: AnyObject]){
        let ref = FIRDatabase.database().reference(fromURL: "https://punotifi.firebaseio.com/")
        let userReference = ref.child("users").child(uid)
        
        userReference.updateChildValues(value) { (err, ref) in
            if err != nil{
                print(err!)
            }
        }
    }
