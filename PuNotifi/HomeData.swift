//
//  HomeData.swift
//  PuNotifi
//
//  Created by Narith on 1/22/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import Foundation
import UIKit

class HomeData{
    struct Entry {
        let imageName: String
        let stationName: String
        let color: UIColor
        
        init(imageName: String, stationName: String, color: UIColor) {
            self.imageName = imageName
            self.stationName = stationName
            self.color = color
        }
    }
    
    let station = [
        Entry(imageName: "police", stationName: "Police", color: UIColor(red: 245/255, green: 166/255, blue: 35/255, alpha: 1)),
        Entry(imageName: "firefighter.png", stationName: "FireFighter", color: UIColor(red: 84/255, green: 199/255, blue: 252/255, alpha: 1)),
        Entry(imageName: "ambulance.png", stationName: "Ambulance", color: UIColor(red: 68/255, green: 219/255, blue: 94/255, alpha: 1)),
        Entry(imageName: "EdC-Logosmall.png", stationName: "EDC", color: UIColor(red: 254/255, green: 40/255, blue: 81/255, alpha: 1))
    ]
}


