//
//  NotifDetailViewController.swift
//  PuNotifi
//
//  Created by BENITEN on 1/27/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase

class NotifDetailViewController: UIViewController {

    var id: Push!
    var user = [User]()
    var userPhotos = ""
    
    @IBOutlet weak var pushImageView: UIImageView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var descLabel: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchData()
        title = "Notification Detail"
        self.navigationController?.navigationBar.tintColor = UIColor.white
        pushImageView.loadImageUsingCacheWithUrlString(id.imageURL)
        nameLabel.text = id.author
//        userPhoto.loadImageUsingCacheWithUrlString(userPhotos)
        descLabel.text = id.desc
        
        let lat = Double(id.lat)
        let lon = Double(id.lon)
        mapView.camera = GMSCameraPosition.camera(withLatitude: lat!, longitude: lon!, zoom: 15.0)

        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: lon!)
        marker.title = id.description
        marker.snippet = id.desc
        marker.map = mapView
        
        
    }

    func fetchData(){
        FIRDatabase.database().reference().child("users").child(id.uid).child("photo").observe(.childAdded, with: { (snapshot) in
            self.userPhotos = snapshot.value as! String
            print("This is image url: " + self.userPhotos)
        }, withCancel: nil)
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func doneButton(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

   
}
