//
//  AppDelegate.swift
//  PuNotifi
//
//  Created by Narith on 12/22/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import GoogleMaps


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
       
        UINavigationBar.appearance().barTintColor = UIColor(red: 84/255, green: 199/255, blue: 252/255, alpha: 1.0)
        UINavigationBar.appearance().tintColor = UIColor.white
        
        // Change the status bar's appearance
        UIApplication.shared.statusBarStyle = .lightContent
        
        // Change the appearance of the tab bar
        UITabBar.appearance().tintColor = UIColor(red: 244.0/255.0, green: 136.0/255.0, blue: 139.0/255.0, alpha: 1.0)
        UITabBar.appearance().barTintColor = UIColor.white

        // Use Firebase library to configure APIs
        FIRApp.configure()
        
        GMSServices.provideAPIKey("AIzaSyCfnxwTlupan8rFN642GeAEQYjUNOM1GWU")
        
//        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }

}

