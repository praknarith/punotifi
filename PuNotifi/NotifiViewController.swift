//
//  NotifiViewController.swift
//  PuNotifi
//
//  Created by Narith on 1/25/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabaseUI


class NotifiViewController: UITableViewController {
    
    let cellId = "cellId"
    var push = [Push]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.tableView.reloadData()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        
        fetchImage()
    }
    
    func fetchImage(){
        FIRDatabase.database().reference().child("posts").observe(.childAdded, with: { (snapshot) in
            if let dictionary = snapshot.value as? [String: AnyObject]{
                let push = Push()
                push.setValuesForKeys(dictionary)
                self.push.append(push)
                
                //avoid app crash while sync
                DispatchQueue.main.async(execute: { 
                    self.tableView.reloadData()
                })
            }
            
        }, withCancel: nil)
    }
    
     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return push.count
    }
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! NotifiViewCell
        let push = self.push[indexPath.row]
        
        
        cell.backgroundColor = UIColor.clear
        cell.labelType.text = push.desc
        cell.imageViewss.loadImageUsingCacheWithUrlString(push.imageURL)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 234
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetailNoti", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailNoti" {
            var indexPath = self.tableView.indexPathForSelectedRow!
            
            let destinatidsonCV = segue.destination as! UINavigationController
            let notiDetail = destinatidsonCV.childViewControllers.first as! NotifDetailViewController
            
            notiDetail.id = push[indexPath.row]
        }
    }
}
