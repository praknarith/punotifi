//
//  postVC.swift
//  PuNotifi
//
//  Created by mengchhorng on 1/23/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class postVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, UITextViewDelegate{

    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var backToCamera: UIButton!
    @IBOutlet weak var sentButton: UIButton!
    @IBOutlet weak var descriptionView: UITextView!
    
    var ref: FIRDatabaseReference!
    
    var imagePassed = UIImage()
    
    let manager = CLLocationManager()
    var lat: Double = 0.0
    var lon: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref = FIRDatabase.database().reference()
        
        postImage.image = imagePassed
        
        backToCamera.layer.cornerRadius = 40
        sentButton.layer.cornerRadius = 10
        
        // manager on mapkit
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        
        //set boarder color
        let myColor : UIColor = UIColor( red: 0.5, green: 0.5, blue:0, alpha: 1.0 )
        descriptionView.layer.masksToBounds = true
        descriptionView.layer.borderColor = myColor.cgColor
        descriptionView.layer.borderWidth = 2.0
        descriptionView.layer.cornerRadius = 5
        
        descriptionView.delegate = self
        hideKeyboardWhenTappedAround()
            
    
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func backToHome(){
        self.dismiss(animated: true, completion: nil)
    }
    // function open camera
    @IBAction func takePicAgain(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    // check after choos photo
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            let Image = info[UIImagePickerControllerOriginalImage] as? UIImage
            postImage.image = Image
            imagePassed = Image!
            self.dismiss(animated: true, completion: nil)
            
        }
    
    // get latitude and longitude
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        lat = location.coordinate.latitude
        lon = location.coordinate.longitude
       }

    @IBAction func sendActionButton(_ sender: Any) {
        let userID = FIRAuth.auth()?.currentUser?.uid
        let user = FIRAuth.auth()?.currentUser?.displayName
        ref.child("users").child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            let _ = snapshot.value as? NSDictionary
            
            self.writeNewPost(withUserID: userID!, username: user!, desc: self.descriptionView.text!)
            _ = self.navigationController?.popViewController(animated: true)
        }) { (error) in
            print(error.localizedDescription)
            
            let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func writeNewPost(withUserID userID: String, username: String, desc: String) {
        let lat: String = String(format: "%f", self.lat)
        let lon: String = String(format: "%f", self.lon)

        let date = NSDate()
        let calendar = NSCalendar.current
        let hour = calendar.component(.hour, from: date as Date)
        let hours: String = String(format: "%f", hour)
        
        let imageName = UUID().uuidString
        let storageRef = FIRStorage.storage().reference().child("postImages").child("\(imageName + hours).png")
    
        if let uploadImage = UIImagePNGRepresentation(self.postImage.image!){
            storageRef.put(uploadImage, metadata: nil, completion: { (metadata, err) in
                
                if err != nil {
                    print(err!)
                }
                if let imageUrl = metadata?.downloadURL()?.absoluteString {
                    let key = self.ref.child("push").childByAutoId().key
                    let post = ["uid": userID,
                                "author": username,
                                "desc": desc,
                                "imageURL": imageUrl,
                                "lat": lat,
                                "lon": lon
                    ]
                    
                    let childUpdates = ["/posts/\(key)": post, "/user-push/\(userID)/\(key)/": post]
                    self.ref.updateChildValues(childUpdates)

                }
            })
        }
        
}

    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        if(text == "\n")
        {
            view.endEditing(true)
            return false
        }
        else
        {
            return true
        }
    }

    override func dismissKeyboard (){
        descriptionView.resignFirstResponder()
    }
    
    
}
