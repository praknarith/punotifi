//
//  User.swift
//  PuNotifi
//
//  Created by BENITEN on 1/24/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit

class User: NSObject {
    var username: String
    var photo: String
    
    init(username: String, photo: String) {
        self.username = username
        self.photo = photo
    }
    
    convenience override init() {
        self.init(username:  "", photo: "")
    }

}
