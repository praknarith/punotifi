//
//  ViewController.swift
//  PuNotifi
//
//  Created by Narith on 12/22/16.
//  Copyright © 2016 praknarith. All rights reserved.
//

import UIKit
import FirebaseAuth


class HomeScreen: BaseViewController, UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let data = HomeData()
  
    
    @IBOutlet weak var loginButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = .black
        
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "background.jpg")!)
        
        updateUI()
        NotificationCenter.default.addObserver(self, selector: #selector(finishLogin), name: Notification.Name(rawValue: "finishLogin"), object: nil)
        
        tableView.delegate = self
        tableView.dataSource = self
        addChildView("HomeScreenID", titleOfChildren: "Home", iconName: "home-1")
        addChildView("NotifiSID", titleOfChildren: "Notification", iconName: "icon")
        addChildView("Contact", titleOfChildren: "Contact", iconName: "call")
        addChildView("About", titleOfChildren: "About", iconName: "about")
        
        
        
    }
    
    func finishLogin(){
        NotificationCenter.default.removeObserver(self)
        loginButton.title = "Log Out"
    }
    
    func updateUI(){
        if FIRAuth.auth()?.currentUser != nil {
            loginButton.title = "Log Out"
        }else{
            loginButton.title = "Log In"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.station.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTableViewCell
        
        //Set cell view to tranparent and round with shadow
        cell.backgroundColor = UIColor.clear
        cell.frameView.layer.cornerRadius = 10
        cell.frameView.layer.shadowColor = UIColor.black.cgColor
        cell.frameView.layer.shadowRadius = 5.0
        cell.frameView.layer.shadowOpacity = 0.4
        
        let entry = data.station[indexPath.row]
        let image = UIImage(named: entry.imageName)
        cell.logoImage.image = image
        cell.nameLabel.text = entry.stationName
        cell.frameView.backgroundColor = entry.color
        
       

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let Image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.dismiss(animated: true) {
            self.gotoVC(image: Image!)
        }
    }
    @IBAction func loginButton(_ sender: Any) {
        if FIRAuth.auth()?.currentUser != nil {
            let firebaseAuth = FIRAuth.auth()
            do {
                try firebaseAuth?.signOut()
                loginButton.title = "Log In"
                self.performSegue(withIdentifier: "gotoLogin", sender: nil)
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
        } else {
            self.performSegue(withIdentifier: "gotoLogin", sender: nil)
        }
    }
    
    func gotoVC(image: UIImage) {
        
        let pvc = storyboard?.instantiateViewController(withIdentifier: "postID") as! postVC
        
        pvc.imagePassed = image
        
        navigationController?.pushViewController(pvc, animated: true)
    }

    
    

}

