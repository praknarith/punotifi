//
//  PageVC.swift
//  PuNotifi
//
//  Created by mengchhorng on 1/19/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit
import FirebaseAuth

class PageVC: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    lazy var VCArr: [UIViewController] = {
        
        return [self.VCInstance("FirstVC"),
                self.VCInstance("SecondVC"),
//                self.VCInstance(name: "ThirdVC"),
//                self.VCInstance(name: "FourthVC"),
//                self.VCInstance(name: "FifthVC"),
//                self.VCInstance(name: "SixthVC"),
//                self.VCInstance(name: "SeventhVC"),
//                self.VCInstance(name: "EighthVC"),
//                self.VCInstance(name: "NinethVC"),
                self.VCInstance("TenthVC")
        ]
    }()
    
    fileprivate func VCInstance(_ name: String) -> UIViewController{
        
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barStyle = .default
        self.delegate = self
        self.dataSource = self
        self.navigationController?.isNavigationBarHidden = true
        
        if FIRAuth.auth()?.currentUser != nil {
            self.performSegue(withIdentifier: "gotoHome", sender: nil)
            return
        }
        
        if let firstVC = VCArr.first{
            
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews {
            
            if view is UIScrollView {
                view.frame = UIScreen.main.bounds
            } else if view is UIPageControl {
                view.backgroundColor = UIColor.clear
            }
            
        }
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = VCArr.index(of: viewController) else {
            
            return nil
        }
        
        let previouseIndex = viewControllerIndex - 1
        
        guard previouseIndex >= 0 else {
            return VCArr.last
        }
        
        guard VCArr.count >= previouseIndex else {
            return nil
        }
        
        return VCArr[previouseIndex]
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = VCArr.index(of: viewController) else {
            
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < VCArr.count else {
            return VCArr.first
        }
        
        guard VCArr.count >= nextIndex else {
            return nil
        }
        
        return VCArr[nextIndex]
        
    }
    
    public func presentationCount(for pageViewController: UIPageViewController) -> Int // The number of items reflected in the page indicator.
    {
        
        return VCArr.count
    }
    
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int // The selected item reflected in the page indicator.
    {
        
        guard let firstViewController = viewControllers?.first, let firstViewControllerIndex = VCArr.index(of: firstViewController) else {
            return 0
        }
        return firstViewControllerIndex
    }
}
