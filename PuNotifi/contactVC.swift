//
//  contactVC.swift
//  PuNotifi
//
//  Created by mengchhorng on 1/25/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit

class contactVC: UIViewController, UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var contactTable: UITableView!
    
    var name = ["Phnom Penh Police Station",
                "Police Khan Chamkarmon",
                "Police Khan Daun Penh",
                "Police Khan Posenchey ",
                "Police Khan Mean Chey ",
                "Police Khan Dangkor ",
                "Police Khan Russey Keo ",
                "Police Khan 7 Makara ",
                "Police Khan Tuol Kork ",
                "Police Khan Char Ampov ",
                "Police Wat Phnom ",
                "Police Khan Sen Sok ",
                "Police Khan Chroy Changvar ",
                
                "Royal Phnom Penh Hospital ",
                "Calmette Hospital",
                "Khema Clinic ",
                "Naga Clinic ",
                "Russian Hospital Ambulance ",
                "Kossamak Hospital Ambulance ",
                "Cho Ray ",
                
                "Toul Sleng Fire Station ",
                "Pochentong Fire Station ",
                "EDC "
    ]
    
    var number = [" 023 722 353",
                  "023 722 153",
                  "011 684 207",
                  "012 325 555",
                  "012 879 897",
                  "017 323 212",
                  "012 776 365",
                  "097 7780 027 / 023 723 053 ",
                  "023 884 163 / 012 232425",
                  "023 321 9906",
                  "097 979 7340",
                  "097 778 0033",
                  "012 845 616",
                  "023 991 000",
                  "023 426 948 / 092 858 434",
                  "023 880 949",
                  "023 211 300",
                  "023 217 764",
                  "016 909 774",
                  "023 684 1001",
                  "023 723 555 / 012 786 693",
                  "023 862 800",
                  "023 723 871"
    ]
    
    var photo = [UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 UIImage(named:"police"),
                 
                 UIImage(named:"ambulance"),
                 UIImage(named:"ambulance"),
                 UIImage(named:"ambulance"),
                 UIImage(named:"ambulance"),
                 UIImage(named:"ambulance"),
                 UIImage(named:"ambulance"),
                 UIImage(named:"ambulance"),
                 
                 UIImage(named:"firefighter"),
                 UIImage(named:"firefighter"),
                 UIImage(named:"EdC-Logosmall"),
                 ]

    override func viewDidLoad() {
        super.viewDidLoad()

        self.contactTable.delegate = self
        self.contactTable.dataSource = self
        
        
      
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = contactTable.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! contactTableViewCell
        
        cell.name.text = self.name[indexPath.row]
        cell.number.text = self.number[indexPath.row]
        cell.img.image = self.photo[indexPath.row]
              
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.separatorInset = UIEdgeInsetsMake(5, 0, 5, 0)
    }
    
}
