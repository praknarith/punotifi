//
//  DataService.swift
//  PuNotifi
//
//  Created by BENITEN on 1/24/17.
//  Copyright © 2017 praknarith. All rights reserved.
//

import UIKit

class Push: NSObject {
    var uid: String
    var author: String
    var desc: String
    var imageURL: String
    var lat: String
    var lon: String
    
    init(uid: String, author: String  ,imageURL: String, desc: String, lat: String, lon: String) {
        self.uid = uid
        self.author = author
        self.desc = desc
        self.imageURL = imageURL
        self.lat = lat
        self.lon = lon
    }
    
    convenience override init() {
        self.init(uid: "",author: "", imageURL: "", desc: "", lat: "", lon: "")
    }
}
